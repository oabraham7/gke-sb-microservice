package com.epam.gke.sb.ms.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.gke.sb.ms.config.ConfigProperties;
import com.epam.gke.sb.ms.pojo.Root;
import com.epam.gke.sb.ms.util.Cons;

@RestController
public class RootController {
	
	@Autowired
	ConfigProperties props;
	
	public static final Logger log = LoggerFactory.getLogger(RootController.class);
	
	@GetMapping("/")
	public Root getStatus() {
		return new Root(Cons.OK_STATUS, Cons.OK_STATUS_MSG + props.getConfigValue("app.version"));
	}
	
}