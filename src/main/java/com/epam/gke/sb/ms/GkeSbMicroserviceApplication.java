package com.epam.gke.sb.ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GkeSbMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(GkeSbMicroserviceApplication.class, args);
	}

}