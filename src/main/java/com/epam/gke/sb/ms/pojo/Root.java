package com.epam.gke.sb.ms.pojo;

public class Root {
	
	private final String status;
	private final String msg;
	
	public Root(String status, String msg) {
		this.status = status;
		this.msg = msg;
	}

	public String getStatus() {
		return status;
	}

	public String getMsg() {
		return msg;
	}
	
}