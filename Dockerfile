FROM openjdk:11
WORKDIR /var/app
COPY target/*.jar /var/app/gke-sb-app.jar
ENTRYPOINT ["java","-jar","/var/app/gke-sb-app.jar"]
EXPOSE 8080
