# GKE Spring-Boot Microservice

Gitlab (gitlab.com) Repository created to store a Maven Java SpringBoot Microservice / Application used for a new CI/CD Process used to document within Disney MH GKE Standards Confluence main page created by Omar Abraham on ORCH project, the Microservice will be also helpful in JFrog Artifactory 7 presentation (EPAM - Tech Talk). 

Disney CI/CD Process confluence page:

* **https://confluence.disney.com/pages/viewpage.action?pageId=703995284**

Disney GKE Standards confluence page:

* **https://confluence.disney.com/display/DTCITechInfraEng/GKE%3A+Standards**

## Architecture Diagram (GSuite diagrams.net)

The current & complete architecture of the CI/CD Process related to this Maven Java Project is:

![image info](doc/diagram/architecture/mh-gke-java-cicd-process.png)

## Microservice JAR execution (Java 11)
	
```
java -jar gke-sb-microservice-$ARTIFACT_VERSION.jar
curl -ILs http://localhost:8080 | curl -vv http://localhost:8080; echo
```

## Docker pull & execution (GCP Artifact Registry Docker image)

```
docker pull us-central1-docker.pkg.dev/$PROJECT_ID/devops/gke-sb-microservice:$IMAGE_TAG
docker run -p 80:8080 -d us-central1-docker.pkg.dev/$PROJECT_ID/devops/gke-sb-microservice:$IMAGE_TAG
curl -ILs http://localhost:80 | curl -vv http://localhost:80; echo
```

## SpringBoot Reference Documentation

For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.5.6/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.5.6/maven-plugin/reference/html/#build-image)

## Master Author

**Omar Abraham:** - <a href="mailto:omar_abraham@epam.com?Subject=Java/Deploy - Doubt" target="_top">**omar_abraham@epam.com**</a>
